create database if not exists sysCompras;
use sysCompras;

-- tablas
create table if not exists departamentos(
idDepartamento int primary key not null auto_increment,
nombre varchar(100) not null
);

create table if not exists empleados(
idEmpleado int primary key not null auto_increment,
nombre varchar(60) not null,
apellido varchar(60) not null,
edad int not null,
cargo varchar(100) not null
);

create table if not exists usuarios(
idUsuario int primary key not null auto_increment,
usuario varchar(30) not null,
clave varchar(16) not null,
edad int not null,
cargo varchar(100) not null
);

create table if not exists articulos(
idArticulo int primary key not null auto_increment,
nombre varchar(80) not null,
stock int not null,
descripcion varchar(100) not null
);

